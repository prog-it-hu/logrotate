<# CONFIG SECTION #>
$Directory = @(
    [pscustomobject]@{
        path = "c:\temp\logsrc";
        fileMask = "*.log";
        nubmersToKeep = 3;
    },
    [pscustomobject]@{
        path = "c:\temp\another-log-src";
        fileMask = "*.log";
        nubmersToKeep = 3;
    }

)
<#! END OF CONFIG SECTION !#>

foreach($dir in $Directory){
    $dirContent = Get-ChildItem $dir.path  | Where-Object{$_.name -like $dir.fileMask} | Sort-Object -Property LastWriteTime
    for($i = 0; $i -lt ($dirContent.Length)-$dir.nubmersToKeep; $i++){
        $dirContent[$i].Delete();
    }
}